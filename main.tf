#Dashboard code updated
terraform {
  required_providers {
    grafana = {
      source = "grafana/grafana"
      version = "1.13.4"
    }
  }
}

provider "grafana" {
  url  = "https://g-38ef978816.grafana-workspace.us-east-1.amazonaws.com/"
  auth = "eyJrIjoiZXZWMmd4Ym9zNzhpTm1vcUhValp6MG9tem9lMVlqVzciLCJuIjoia2FuYXBhdGhpIiwiaWQiOjF9"
}
#Folder Creations

resource "grafana_folder" "collection" {
  title = "SMART-Dashboards-Home"
}

resource "grafana_folder" "OSD" {
  title = "SMART-Dashboards"
}

resource "grafana_folder" "Alerting" {
  title = "SMART-Dashboards-Alerting"
}
